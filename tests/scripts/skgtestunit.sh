#!/bin/sh
EXE=skgtestunit

#initialisation
. "`dirname \"$0\"`/init.sh"

export PATH=${IN}/../../skgbankmodeler:${PATH}

skrooge-yahoodl.py AIR.PA 20170801 20170809 1d
rc=$?
if [ $rc != 0 ] ; then
	exit $rc
fi

"${EXE}"
rc=$?
if [ $rc != 0 ] ; then
	exit $rc
fi

exit 0
