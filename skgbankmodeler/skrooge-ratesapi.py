#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#***************************************************************************
#* SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************

import urllib.request
import json
import sys
import time

args = sys.argv[1].split('/')
url = 'https://api.ratesapi.io/api/latest?base='+args[0]+'&symbols='+args[1]
req = urllib.request.Request(url, headers={'User-Agent': 'Mozilla/5.0'})
f = urllib.request.urlopen(req)
print("Date,Price")
data = json.loads(f.read().decode('utf-8'))
print(data["date"]+","+str(data["rates"][args[1]]))

