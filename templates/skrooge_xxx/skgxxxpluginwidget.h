/***************************************************************************
 * SPDX-FileCopyrightText: 2022 S. MANKOWSKI stephane@mankowski.fr
 * SPDX-FileCopyrightText: 2022 G. DE BURE support@mankowski.fr
 * SPDX-License-Identifier: GPL-3.0-or-later
 ***************************************************************************/
#ifndef SKGXXXPLUGINWIDGET_H
#define SKGXXXPLUGINWIDGET_H
/** @file
 * ##DESCRIPTION##
*
* @author ##AUTHOR##
*/
#include "ui_skgxxxpluginwidget_base.h"
#include "skgtabpage.h"

/**
 * ##DESCRIPTION##
 */
class SKGXXXPluginWidget : public SKGTabPage
{
    Q_OBJECT

public:
    /**
     * Default Constructor
     * @param iParent the parent widget
     * @param iDocument the document
     */
    explicit SKGXXXPluginWidget(QWidget* iParent, SKGDocument* iDocument);

    /**
     * Default Destructor
     */
    virtual ~SKGXXXPluginWidget();

    /**
     * Get the current state
     * MUST BE OVERWRITTEN
     * @return a string containing all information needed to set the same state.
     * Could be an XML stream
     */
    virtual QString getState() override;

    /**
     * Set the current state
     * MUST BE OVERWRITTEN
     * @param iState must be interpreted to set the state of the widget
     */
    virtual void setState(const QString& iState) override;

    /**
     * Get attribute name to save the default state
     * MUST BE OVERWRITTEN
     * @return attribute name to save the default state.
     */
    virtual QString getDefaultStateAttribute() override;

    /**
     * Get the main widget
     * @return a widget
     */
    virtual QWidget* mainWidget() override;

public Q_SLOTS:
    /**
    * Refresh the content.
     */
    virtual void refresh();

private Q_SLOTS:


private:
    Q_DISABLE_COPY(SKGXXXPluginWidget)

    Ui::skgxxxplugin_base ui;
};

#endif  // SKGXXXPLUGINWIDGET_H
